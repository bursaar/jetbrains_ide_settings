# JetBrains' IDE Settings

Contains settings for the following IDEs:

- Rider (C#)
- PyCharm (Python)
- Android Studio (Java)